/*
 * SheenshIn engine hotkeys module.
 * (C) 2016 Lupshenko.
 * This code is licensed under the GPL3.
 */

game.hotkeys = {
	//Secondary variables
		pressed: [],
		released: [],
	
	//Methods
		catch: function(keyCode, isDown) {
			[
				this.released,
				this.pressed
			][+isDown].push(keyCode);
		}
};
