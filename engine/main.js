/*
 * SheenshIn engine main module.
 * (C) 2016 Lupshenko.
 * This code is licensed under the GPL3.
 */

var
	fs = require("fs"),
	win = nw.Window.get(),
	
	game = {
		//Secondary variables
			screen: {
				width: 320,
				height: 240
			},
			
			loadedTexturesCount: 0,
			loadedSoundsCount: 0,
			
			realms: {},
		
		//Methods
			init: function() {
				this.config.read();
				
				//Load resources
					this.loadTextures();
					this.loadSounds();
				
				//Setup canvas
					this.canvasElement = document.getElementById("canvas");
					this.canvasContext = this.canvasElement.getContext("2d");
					
					this.canvasContext.imageSmoothingEnabled = false;
				
				//Setup game world
					
				
				this.start();
			},
			
			start: function() {
				if (
					this.loadedTexturesCount ==
						Object.keys(this.textures).length &&
					this.loadedSoundsCount ==
						Object.keys(this.sounds).length
				) {
					this.sounds.background.play();
					
					document.getElementById("loadingScreen").remove();
					
					this.curRealm = this.realms.menu;
					
					this.realms.gameWorld.curObject =
						this.realms.gameWorld.objects.player;
					
					this.update();
				} else {
					setTimeout("game.start()", 100);
				}
			},
			
			update: function() {
				var layers = this.curRealm.update();
				
				this.canvasContext.clearRect(
					0,
					0,
					
					this.canvasElement.width,
					this.canvasElement.height
				);
				
				for (var i = 0; i < layers.length; i++) {
					for (var j = 0; j < layers[i].length; j++) {
						switch (layers[i][j].type) {
							case "sprite":
								this.canvasContext.drawImage(
									layers[i][j].texture,
									
									layers[i][j].x *
									(innerWidth / this.screen.width),
									layers[i][j].y *
									(innerHeight / this.screen.height),
									
									(layers[i][j].width || layers[i][j].texture.width) *
									(innerWidth / this.screen.width),
									(layers[i][j].height || layers[i][j].texture.height) *
									(innerHeight / this.screen.height)
								);
								
								break;
							case "text":
								this.canvasContext.textAlign = layers[i][j].textAlign; 
								
								this.canvasContext.font =
									layers[i][j].fontSize *
									(innerWidth / this.screen.height) + "px " +
									layers[i][j].fontFamily;
								
								this.canvasContext.fillStyle = layers[i][j].fontColor;
								
								this.canvasContext.fillText(
									layers[i][j].string,
									
									layers[i][j].x *
									(innerWidth / this.screen.width),
									layers[i][j].y *
									(innerHeight / this.screen.height)
								);
								
								break;
							default:
								console.log(new TypeError(
									"type \"" +
									layers[i][j].type +
									"\" doesn't exist"
								));
						}
					}
				}
				
				setTimeout("game.update()", 1);
			}
	};
