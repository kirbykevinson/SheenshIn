/*
 * SheenshIn engine main module textures loading method.
 * (C) 2016 Lupshenko.
 * This code is licensed under the GPL3.
 */

game.loadTexture = function(pathToImage) {
	var imageElement = new Image();
	imageElement.src = pathToImage;
	
	imageElement.addEventListener("load", function() {
		game.loadedTexturesCount++;
	});
	
	return imageElement;
};

game.loadTextures = function() {
	this.textures = {
		//Common
			button:
				this.loadTexture("textures/button.png"),
			button_selected:
				this.loadTexture("textures/button_selected.png"),
		
		//Menu
			menu:
				this.loadTexture("textures/menu.png"),
			menu_background:
				this.loadTexture("textures/menu_background.png"),
		
		//Authors
			authors:
				this.loadTexture("textures/authors.png"),
		
		//Game world
			gameWorld:
				this.loadTexture("textures/gameWorld.png"),
			gameWorld_background:
				this.loadTexture("textures/gameWorld_background.png"),
			
			gameWorld_player_down:
				this.loadTexture("textures/gameWorld_player_down.png"),
			gameWorld_player_down_1:
				this.loadTexture("textures/gameWorld_player_down_1.png"),
			gameWorld_player_left:
				this.loadTexture("textures/gameWorld_player_left.png"),
			gameWorld_player_left_1:
				this.loadTexture("textures/gameWorld_player_left_1.png"),
			gameWorld_player_up:
				this.loadTexture("textures/gameWorld_player_up.png"),
			gameWorld_player_up_1:
				this.loadTexture("textures/gameWorld_player_up_1.png"),
			gameWorld_player_right:
				this.loadTexture("textures/gameWorld_player_right.png"),
			gameWorld_player_right_1:
				this.loadTexture("textures/gameWorld_player_right_1.png"),
			
			gameWorld_pc:
				this.loadTexture("textures/gameWorld_pc.png"),
			gameWorld_pc_gameReadinessBar:
				this.loadTexture("textures/gameWorld_pc_gameReadinessBar.png"),
			gameWorld_pcWithPlayer:
				this.loadTexture("textures/gameWorld_pcWithPlayer.png"),
			
			gameWorld_bed:
				this.loadTexture("textures/gameWorld_bed.png"),
			gameWorld_bed_tirednessBar:
				this.loadTexture("textures/gameWorld_bed_tirednessBar.png"),
			gameWorld_bedWithPlayer:
				this.loadTexture("textures/gameWorld_bedWithPlayer.png"),
			
			gameWorld_toilet:
				this.loadTexture("textures/gameWorld_toilet.png"),
			gameWorld_toilet_shitLevelBar:
				this.loadTexture("textures/gameWorld_toilet_shitLevelBar.png"),
			gameWorld_toiletWithPlayer:
				this.loadTexture("textures/gameWorld_toiletWithPlayer.png"),
			
			gameWorld_cat:
				this.loadTexture("textures/gameWorld_cat.png"),
			gameWorld_cat_shitLevelBar:
				this.loadTexture("textures/gameWorld_cat_shitLevelBar.png"),
			gameWorld_cat_1:
				this.loadTexture("textures/gameWorld_cat_1.png"),
			gameWorld_cat_2:
				this.loadTexture("textures/gameWorld_cat_2.png"),
			gameWorld_cat_3:
				this.loadTexture("textures/gameWorld_cat_3.png"),
			gameWorld_opyatNaPolNasralPidoras:
				this.loadTexture("textures/gameWorld_opyatNaPolNasralPidoras.png"),
			gameWorld_opyatNaPolNasralPidoras_1:
				this.loadTexture("textures/gameWorld_opyatNaPolNasralPidoras_1.png"),
		
		//Game over
			gameOver:
				this.loadTexture("textures/gameOver.png"),
		
		//You won
			youWon:
				this.loadTexture("textures/youWon.png")
	};
};
