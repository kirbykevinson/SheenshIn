/*
 * SheenshIn engine main module sounds loading method.
 * (C) 2016 Lupshenko.
 * This code is licensed under the GPL3.
 */

game.loadSound = function(pathToAudioFile) {
	var audioElement = new Audio();
	audioElement.src = pathToAudioFile;
	
	audioElement.addEventListener("loadstart", function() {
		game.loadedSoundsCount++;
	});
	
	return audioElement;
};

game.loadSounds = function() {
	this.sounds = {
		//Common
			background:
				this.loadSound("sounds/background.wav"),
			
			click:
				this.loadSound("sounds/click.wav"),
			scroll:
				this.loadSound("sounds/scroll.wav"),
		
		//Game world
			gameWorld_win:
				this.loadSound("sounds/gameWorld_win.wav"),
			gameWorld_fail:
				this.loadSound("sounds/gameWorld_fail.wav"),
			
			gameWorld_player_walking:
				this.loadSound("sounds/gameWorld_player_walking.wav"),
			
			gameWorld_pcWithPlayer:
				this.loadSound("sounds/gameWorld_pcWithPlayer.wav"),
			gameWorld_pcWithPlayer_foodEating:
				this.loadSound("sounds/gameWorld_pcWithPlayer_foodEating.wav"),
			
			gameWorld_bedWithPlayer:
				this.loadSound("sounds/gameWorld_bedWithPlayer.wav"),
			
			gameWorld_toiletWithPlayer:
				this.loadSound("sounds/gameWorld_toiletWithPlayer.wav"),
			
			gameWorld_cat_pooping:
				this.loadSound("sounds/gameWorld_cat_pooping.wav"),
			gameWorld_opyatNaPolNasralPidoras:
				this.loadSound("sounds/gameWorld_opyatNaPolNasralPidoras.wav")
	};
	
	this.sounds.background.loop = true;
};
