/*
 * SheenshIn engine config module.
 * (C) 2016 Lupshenko.
 * This code is licensed under the GPL3.
 */

game.config = {
	//Secondary variables
		list: {},
		defaultList: {
			main: {
				hotkeys: {
					down: 83, //"S"
					left: 65, //"A"
					up: 87, //"W"
					right: 68,  //"D"
					
					use: 69, //"E"
					menu: 27 //Escape
				}
			},
			
			gameWorld: {
				timer: 0,
				
				player: {
					position: {
						x: 100,
						y: 140
					},
					rotation: 0,
					step: 0,
				},
				
				pc: {
					gameReadiness: 0
				},
				
				bed: {
					tiredness: 0
				},
				
				toilet: {
					shitLevel: 0,
				},
				
				cat: {
					shitLevel: 0,
					
					step: 0
				}
			}
		},
		
		pathPrefix: "config/",
		paths: {
			main: "main.json",
			gameWorld: "gameWorld.json"
		},
		
	//Methods
		read: function() {
			this.list = JSON.parse(JSON.stringify(
				this.defaultList
			));
			
			for (var i in this.list) {
				var curConfig = "";
				try {
					curConfig = fs.readFileSync(
						this.pathPrefix +
						this.paths[i],
					"utf-8");
				} catch(e) {}
				
				if (curConfig) {
					try {
						this.list[i] = JSON.parse(curConfig);
					} catch(e) {
						alert(new ReferenceError(
							"can't read " +
							this.pathPrefix +
							this.paths[i] +
							
							" (" + e + ")"
						));
					}
				}
			}
		},
		
		write: function(parameter) {
			try {
				fs.writeFileSync(
					this.pathPrefix +
					this.paths[parameter],
					
					JSON.stringify(this.list[parameter])
				);
			} catch(e) {
				alert(new ReferenceError(
					"can't write " +
					this.pathPrefix +
					this.paths[parameter] +
					
					" (" + e + ")"
				));
			}
		}
};
