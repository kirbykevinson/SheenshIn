/*
 * SheenshIn game world realm.
 * (C) 2016 Lupshenko.
 * This code is licensed under the GPL3.
 */

game.realms.gameWorld = {
	//Secondary variables
		objects: {},
		
		size: {
			width: 20,
			height: 15
		},
	
	//Methods
		reset: function() {
			game.sounds.gameWorld_pcWithPlayer.pause();
			game.sounds.gameWorld_pcWithPlayer.currentTime = 0;
			game.sounds.gameWorld_bedWithPlayer.pause();
			game.sounds.gameWorld_bedWithPlayer.currentTime = 0;
			game.sounds.gameWorld_opyatNaPolNasralPidoras.pause();
			game.sounds.gameWorld_opyatNaPolNasralPidoras.currentTime = 0;
			game.sounds.gameWorld_toiletWithPlayer.pause();
			game.sounds.gameWorld_toiletWithPlayer.currentTime = 0;
			
			this.curObject =
				this.objects.player;
			
			game.config.list.gameWorld = JSON.parse(JSON.stringify(
				game.config.defaultList.gameWorld
			));
			game.config.write("gameWorld");
		},
		win: function() {
			game.sounds.gameWorld_win.play();
			
			this.reset();
			
			game.curRealm = game.realms.youWon;
		},
		fail: function() {
			game.sounds.gameWorld_fail.play();
			
			this.reset();
			
			game.curRealm = game.realms.gameOver;
		},
		
		updateMap: function() {
			game.config.list.gameWorld.timer++;
			
			for (var i in this.objects) {
				this.objects[i].update();
			}
			
			if (game.curRealm == this) {
				setTimeout("game.realms.gameWorld.updateMap()", 40);
			}
		},
		
		update: function() {
			this.curObject.handleHotkeys();
			
			return [
				this.renderBackground(),
				this.renderObjects(),
				this.renderGui()
			];
		},
		
		renderBackground: function() {
			return [{
				type: "sprite",
				
				texture:
					game.textures.gameWorld_background,
				
				x: 0,
				y: 0
			}];
		},
		renderObjects: function() {
			var layer = [];
			
			for (var i in this.objects) {
				layer.push(this.objects[i].render());
			}
			
			return layer;
		},
		renderGui: function() {
			return [
				{ //Base
					type: "sprite",
					
					texture:
						game.textures.gameWorld,
					
					x: 0,
					y: 0
				},
				
				//Bars
					{ //PC (game readiness)
						type: "sprite",
						
						texture:
							game.textures.gameWorld_pc_gameReadinessBar,
						
						x: 129,
						y: 42,
						
						width:
							game.config.list.gameWorld.pc.gameReadiness * 0.62,
						height: 2
					},
					
					{ //Bed (tiredness)
						type: "sprite",
						
						texture:
							game.textures.gameWorld_bed_tirednessBar,
						
						x: 9,
						y: 96,
						
						width:
							game.config.list.gameWorld.bed.tiredness * 0.62,
						height: 2
					},
					
					{ //Toilet (shit level)
						type: "sprite",
						
						texture:
							game.textures.gameWorld_toilet_shitLevelBar,
						
						x: 241,
						y: 112,
						
						width:
							game.config.list.gameWorld.toilet.shitLevel * 0.62,
						height: 2
					},
					
					{ //Cat (shit level)
						type: "sprite",
						
						texture:
							game.textures.gameWorld_cat_shitLevelBar,
						
						x: 129,
						y: 190,
						
						width:
							game.config.list.gameWorld.cat.shitLevel * 0.62,
						height: 2
					},
				
				{ //Timer
					type: "text",
					
					fontSize: 10,
					fontFamily: "SheenshIn",
					fontColor: "#FFFFFF",
					textAlign: "center",
					
					string:
						game.config.list.gameWorld.timer,
					
					x: 160,
					y: 234
				}
			];
		}
};
