/*
 * SheenshIn game world realm pc object.
 * (C) 2016 Lupshenko.
 * This code is licensed under the GPL3.
 */

game.realms.gameWorld.objects.pc = {
	update: function() {
		if (
			Math.random() < 0.0062475 &&
			game.realms.gameWorld.curObject == this
		) {
			game.sounds.gameWorld_pcWithPlayer_foodEating.play();
		}
		
		if (game.config.list.gameWorld.pc.gameReadiness >= 100) {
			game.realms.gameWorld.win();
		}
	},
	
	handleHotkeys: function() {
		for (var i = 0; i < game.hotkeys.pressed.length; i++) {
			switch (game.hotkeys.pressed[i]) {
				case game.config.list.main.hotkeys.menu:
					game.sounds.gameWorld_pcWithPlayer.pause();
					game.sounds.gameWorld_pcWithPlayer.currentTime = 0;
					game.sounds.gameWorld_player_walking.play();
					
					game.realms.gameWorld.curObject =
						game.realms.gameWorld.objects.player;
					
					break;
				case game.config.list.main.hotkeys.use:
					game.sounds.gameWorld_pcWithPlayer.play();
					
					game.config.list.gameWorld.pc.gameReadiness += 0.0065;
					
					break;
			}
		}
		
		game.hotkeys.pressed.length = 0;
		game.hotkeys.released.length = 0;
	},
	
	render: function() {
		return {
			type: "sprite",
			
			texture: [
				game.textures.gameWorld_pc,
				game.textures.gameWorld_pcWithPlayer
			][+(game.realms.gameWorld.curObject == this)],
			
			x: 112,
			y: 44
		};
	}
};
