/*
 * SheenshIn game world realm toilet object.
 * (C) 2016 Lupshenko.
 * This code is licensed under the GPL3.
 */

game.realms.gameWorld.objects.toilet = {
	update: function() {
		game.config.list.gameWorld.toilet.shitLevel += 0.045;
		
		if (game.config.list.gameWorld.toilet.shitLevel >= 100) {
			game.realms.gameWorld.fail();
		}
	},
	
	handleHotkeys: function() {
		for (var i = 0; i < game.hotkeys.pressed.length; i++) {
			switch (game.hotkeys.pressed[i]) {
				case game.config.list.main.hotkeys.menu:
					game.sounds.gameWorld_toiletWithPlayer.pause();
					game.sounds.gameWorld_toiletWithPlayer.currentTime = 0;
					game.sounds.gameWorld_player_walking.play();
					
					game.realms.gameWorld.curObject =
						game.realms.gameWorld.objects.player;
					
					break;
				case game.config.list.main.hotkeys.use:
					game.sounds.gameWorld_toiletWithPlayer.play();
					
					if (
						game.realms.gameWorld.curObject == this &&
						
						game.config.list.gameWorld.toilet.shitLevel > 0
					) {
						game.config.list.gameWorld.toilet.shitLevel -= 0.27;
					}
					
					break;
			}
		}
		
		game.hotkeys.pressed.length = 0;
		game.hotkeys.released.length = 0;
	},
	
	render: function() {
		return {
			type: "sprite",
			
			texture: [
				game.textures.gameWorld_toilet,
				game.textures.gameWorld_toiletWithPlayer,
			][+(game.realms.gameWorld.curObject == this)],
			
			x: 240,
			y: 122
		};
	}
};
