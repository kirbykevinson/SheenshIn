/*
 * SheenshIn game world realm cat object.
 * (C) 2016 Lupshenko.
 * This code is licensed under the GPL3.
 */

game.realms.gameWorld.objects.cat = {
	update: function() {
		if (Math.random() < 0.0062475) {
			game.sounds.gameWorld_cat_pooping.play();
		}
		
		game.config.list.gameWorld.cat.shitLevel += 0.03;
		
		if (game.config.list.gameWorld.cat.shitLevel >= 100) {
			game.realms.gameWorld.fail();
		}
	},
	
	handleHotkeys: function() {
		for (var i = 0; i < game.hotkeys.pressed.length; i++) {
			switch (game.hotkeys.pressed[i]) {
				case game.config.list.main.hotkeys.menu:
					game.sounds.gameWorld_opyatNaPolNasralPidoras.pause();
					game.sounds.gameWorld_opyatNaPolNasralPidoras.currentTime = 0;
					game.sounds.gameWorld_player_walking.play();
					
					game.realms.gameWorld.curObject =
						game.realms.gameWorld.objects.player;
					
					break;
				case game.config.list.main.hotkeys.use:
					game.sounds.gameWorld_opyatNaPolNasralPidoras.play();
					
					game.config.list.gameWorld.cat.step = [1, 0][
						game.config.list.gameWorld.cat.step
					];
					
					if (
						game.realms.gameWorld.curObject == this &&
						
						game.config.list.gameWorld.cat.shitLevel > 0
					) {
						game.config.list.gameWorld.cat.shitLevel -= 0.18;
					}
					
					break;
			}
		}
		
		game.hotkeys.pressed.length = 0;
		game.hotkeys.released.length = 0;
	},
	
	render: function() {
		if (game.realms.gameWorld.curObject == this) {
			return {
				type: "sprite",
				
				texture: [
					game.textures.gameWorld_opyatNaPolNasralPidoras,
					game.textures.gameWorld_opyatNaPolNasralPidoras_1
				][game.config.list.gameWorld.cat.step],
				
				x: 144,
				y: 160
			};
		} else {
			return {
				type: "sprite",
				
				texture: [
					game.textures.gameWorld_cat,
					game.textures.gameWorld_cat_1,
					game.textures.gameWorld_cat_2,
					game.textures.gameWorld_cat_3
				][Math.floor(
					game.config.list.gameWorld.cat.shitLevel / 25
				)] || game.textures.gameWorld_cat,
				
				x: 144,
				y: 192
			};
		}
	}
};
