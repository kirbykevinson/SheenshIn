/*
 * SheenshIn game world realm bed object.
 * (C) 2016 Lupshenko.
 * This code is licensed under the GPL3.
 */

game.realms.gameWorld.objects.bed = {
	update: function() {
		game.config.list.gameWorld.bed.tiredness += 0.02;
		
		if (game.config.list.gameWorld.bed.tiredness >= 100) {
			game.realms.gameWorld.fail();
		}
	},
	
	handleHotkeys: function() {
		for (var i = 0; i < game.hotkeys.pressed.length; i++) {
			switch (game.hotkeys.pressed[i]) {
				case game.config.list.main.hotkeys.menu:
					game.sounds.gameWorld_bedWithPlayer.pause();
					game.sounds.gameWorld_bedWithPlayer.currentTime = 0;
					game.sounds.gameWorld_player_walking.play();
					
					game.realms.gameWorld.curObject =
						game.realms.gameWorld.objects.player;
					
					break;
				case game.config.list.main.hotkeys.use:
					game.sounds.gameWorld_bedWithPlayer.play();
					
					if (
						game.realms.gameWorld.curObject == this &&
						
						game.config.list.gameWorld.bed.tiredness > 0
					) {
						game.config.list.gameWorld.bed.tiredness -= 0.12;
					}
					
					break;
			}
		}
		
		game.hotkeys.pressed.length = 0;
		game.hotkeys.released.length = 0;
	},
	
	render: function() {
		return {
			type: "sprite",
			
			texture: [
				game.textures.gameWorld_bed,
				game.textures.gameWorld_bedWithPlayer
			][+(game.realms.gameWorld.curObject == this)],
			
			x: 16,
			y: 106
		};
	}
};
