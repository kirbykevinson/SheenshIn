/*
 * SheenshIn game world realm player object.
 * (C) 2016 Lupshenko.
 * This code is licensed under the GPL3.
 */

game.realms.gameWorld.objects.player = {
	update: function() {
		//Ну тут бля типа чет должно быть но я хз че сюда запихать воооот
	},
	
	handleHotkeys: function() {
		for (var i = 0; i < game.hotkeys.pressed.length; i++) {
			switch (game.hotkeys.pressed[i]) {
				case game.config.list.main.hotkeys.menu:
					game.sounds.click.play();
					
					game.config.write("gameWorld");
					
					game.curRealm = game.realms.menu;
					
					break;
				case game.config.list.main.hotkeys.use:
					game.sounds.gameWorld_player_walking.play();
					
					if (
						game.config.list.gameWorld.player.position.y == 168 &&
						game.config.list.gameWorld.player.rotation == 0
					) {
						game.realms.gameWorld.curObject =
							game.realms.gameWorld.objects.cat;
					} else if (
						game.config.list.gameWorld.player.position.x == 64 &&
						game.config.list.gameWorld.player.rotation == 1
					) {
						game.realms.gameWorld.curObject =
							game.realms.gameWorld.objects.bed;
					} else if (
						game.config.list.gameWorld.player.position.y == 140 &&
						game.config.list.gameWorld.player.rotation == 2
					) {
						game.realms.gameWorld.curObject =
							game.realms.gameWorld.objects.pc;
					} else if (
						game.config.list.gameWorld.player.position.x == 208 &&
						game.config.list.gameWorld.player.rotation == 3
					) {
						game.realms.gameWorld.curObject =
							game.realms.gameWorld.objects.toilet;
					}
					
					break;
				case game.config.list.main.hotkeys.down:
					game.sounds.gameWorld_player_walking.play();
					
					game.config.list.gameWorld.player.rotation = 0;
					
					game.config.list.gameWorld.player.step = [1, 0][
						game.config.list.gameWorld.player.step
					];
					
					if (
						game.config.list.gameWorld.player.position.y != 168
					) {
						game.config.list.gameWorld.player.position.y += 4;
					}
					
					break;
				case game.config.list.main.hotkeys.left:
					game.sounds.gameWorld_player_walking.play();
					
					game.config.list.gameWorld.player.rotation = 1;
					
					game.config.list.gameWorld.player.step = [1, 0][
						game.config.list.gameWorld.player.step
					];
					
					if (
						game.config.list.gameWorld.player.position.x != 64
					) {
						game.config.list.gameWorld.player.position.x -= 4;
					}
					
					break;
				case game.config.list.main.hotkeys.up:
					game.sounds.gameWorld_player_walking.play();
					
					game.config.list.gameWorld.player.rotation = 2;
					
					game.config.list.gameWorld.player.step = [1, 0][
						game.config.list.gameWorld.player.step
					];
					
					if (
						game.config.list.gameWorld.player.position.y != 140
					) {
						game.config.list.gameWorld.player.position.y -= 4;
					}
					
					break;
				case game.config.list.main.hotkeys.right:
					game.sounds.gameWorld_player_walking.play();
					
					game.config.list.gameWorld.player.rotation = 3;
					
					game.config.list.gameWorld.player.step = [1, 0][
						game.config.list.gameWorld.player.step
					];
					
					if (
						game.config.list.gameWorld.player.position.x != 208
					) {
						game.config.list.gameWorld.player.position.x += 4;
					}
					
					break;
			}
		}
		
		game.hotkeys.pressed.length = 0;
		game.hotkeys.released.length = 0;
	},
	
	render: function() {
		return {
			type: "sprite",
			
			texture:
				this.texture(),
			
			x: [
				999,
				game.config.list.gameWorld.player.position.x
			][+(game.realms.gameWorld.curObject == this)],
			y: game.config.list.gameWorld.player.position.y - 32
		};
	},
	texture: function() {
		switch (game.config.list.gameWorld.player.step) {
			case 0:
				return [
					game.textures.gameWorld_player_down,
					game.textures.gameWorld_player_left,
					game.textures.gameWorld_player_up,
					game.textures.gameWorld_player_right
				][game.config.list.gameWorld.player.rotation];
			case 1:
				return [
					game.textures.gameWorld_player_down_1,
					game.textures.gameWorld_player_left_1,
					game.textures.gameWorld_player_up_1,
					game.textures.gameWorld_player_right_1
				][game.config.list.gameWorld.player.rotation];
		}
	}
};
