/*
 * SheenshIn you won realm.
 * (C) 2016 Lupshenko.
 * This code is licensed under the GPL3.
 */

game.realms.youWon = {
	//Secondary variables
		
	
	//Methods
		update: function() {
			this.handleHotkeys();
			
			return [[
				{ //Background
					type: "sprite",
					
					texture:
						game.textures.youWon,
					
					x: 0,
					y: 0
				},
				
				{ //"GAME IS DONE"
					type: "text",
					
					fontSize: 24,
					fontFamily: "SheenshIn",
					fontColor: "#FFFFFF",
					textAlign: "center",
					
					string:
						"GAME IS DONE",
					
					x: 160,
					y: 120
				},
				
				{ //"Press Escape to exit"
					type: "text",
					
					fontSize: 16,
					fontFamily: "SheenshIn",
					fontColor: "#FFFFFF",
					textAlign: "center",
					
					string:
						"Press Escape to exit",
					
					x: 160,
					y: 228
				}
			]];
		},
		
		handleHotkeys: function() {
			for (var i = 0; i < game.hotkeys.pressed.length; i++) {
				if (game.hotkeys.pressed[i] ==
					game.config.list.main.hotkeys.menu
				) {
					game.sounds.click.play();
					
					game.curRealm = game.realms.menu;
				}
			}
			
			game.hotkeys.pressed.length = 0;
			game.hotkeys.released.length = 0;
		}
};
