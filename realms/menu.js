/*
 * SheenshIn menu realm.
 * (C) 2016 Lupshenko.
 * This code is licensed under the GPL3.
 */

game.realms.menu = {
	//Secondary variables
		buttons: [
			{ //"Play"
				caption:
					"Play",
				
				x: 184,
				y: 144,
				
				event: function() {
					game.curRealm = game.realms.gameWorld;
					
					game.realms.gameWorld.updateMap();
				}
			},
			
			{ //"Authors"
				caption:
					"Authors",
				
				x: 184,
				y: 174,
				
				event: function() {
					game.curRealm = game.realms.authors;
				}
			},
			
			{ //"Exit"
				caption:
					"Exit",
				
				x: 184,
				y: 204,
				
				event: function() {
					win.close();
				}
			}
		],
		
		animatedShitScroll: 0,
		
		curButton: 0,
	
	//Methods
		update: function() {
			this.handleHotkeys();
			
			return [
				this.renderBase(),
				this.renderButtons()
			];
		},
		
		handleHotkeys: function() {
			for (var i = 0; i < game.hotkeys.pressed.length; i++) {
				switch (game.hotkeys.pressed[i]) {
					case game.config.list.main.hotkeys.up:
						game.sounds.scroll.play();
						
						this.curButton--;
						
						if (this.curButton < 0) {
							this.curButton = this.buttons.length - 1;
						}
						
						break;
					case game.config.list.main.hotkeys.down:
						game.sounds.scroll.play();
						
						this.curButton++;
						
						if (this.curButton >= this.buttons.length) {
							this.curButton = 0;
						}
						
						break;
					case game.config.list.main.hotkeys.use:
						game.sounds.click.play();
						
						this.buttons[this.curButton].event();
						
						break;
				}
			}
			
			game.hotkeys.pressed.length = 0;
			game.hotkeys.released.length = 0;
		},
		
		renderBase: function() {
			return [
				{ //Background
					type: "sprite",
					
					texture:
						game.textures.menu_background,
					
					x: 0,
					y: 0
				},
				
				{ //Logo and other stuff
					type: "sprite",
					
					texture:
						game.textures.menu,
					
					x: 0,
					y: 0
				}
			];
		},
		renderButtons: function() {
			var layer = [];
			
			for (var i = 0; i < this.buttons.length; i++) {
				layer.push(
					{
						type: "sprite",
						
						texture: [
							game.textures.button,
							game.textures.button_selected
						][+(i == this.curButton)],
						
						x: this.buttons[i].x,
						y: this.buttons[i].y
					},
					
					{
						type: "text",
						
						fontSize: 12,
						fontFamily: "SheenshIn",
						fontColor: "#FFFFFF",
						textAlign: "center",
						
						string:
							this.buttons[i].caption,
						
						x: this.buttons[i].x + 60,
						y: this.buttons[i].y + 15
					}
				);
			}
			
			return layer;
		}
};
